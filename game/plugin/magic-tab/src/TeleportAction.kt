import org.apollo.game.action.ActionBlock
import org.apollo.game.action.AsyncAction
import org.apollo.game.model.Animation
import org.apollo.game.model.Graphic
import org.apollo.game.model.Position
import org.apollo.game.model.entity.Player

class TeleportAction(
    val player: Player,
    val destination: Spell
) : AsyncAction<Player>(0, true, player) {

    override fun action(): ActionBlock = {
            mob.playAnimation(TELEPORT_ANIMATION)
            mob.playGraphic(TELEPORT_GRAPHIC)
            wait(pulses = 3)
            player.teleport(Position(destination.coordX, destination.coordY))
            player.stopAnimation()
            player.stopGraphic()
            mob.sendMessage("You teleport to " + destination);
        stop()
    }

    companion object {
        public val TELEPORT_ANIMATION = Animation(714)
        public val TELEPORT_GRAPHIC = Graphic(111, 5, 100)
    }
}