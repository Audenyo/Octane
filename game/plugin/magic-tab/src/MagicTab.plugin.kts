import org.apollo.game.message.impl.ButtonMessage
import org.apollo.game.model.Animation
import org.apollo.game.model.Position

on { ButtonMessage::class }
    .where { widgetId in Spell.MAP }
    .then { player ->
        val spell = Spell.fromButton(widgetId)
        if (spell != null) {
            player.startAction(TeleportAction(player, spell))
        }
        player.sendMessage("Id: " + widgetId)
    }